package factory

import (
	"errors"
	"fmt"
)

func (bahan *Bahan) NasiGoreng(jumlah int) MenuFactory {
	// nasi goreng pake 2 porsi nasi, 1 sayur, 2 telur
	var (
		butuhNasi  = jumlah * 2
		butuhSayur = jumlah * 1
		butuhTelur = jumlah * 2
	)

	if butuhNasi > bahan.Nasi || butuhSayur > bahan.Sayur || butuhTelur > bahan.Telur {
		bahan.ErrMsg = errors.New("Bahan tidak cukup untuk membuat nasi goreng")
		return bahan
	}

	bahan.Nasi = bahan.Nasi - butuhNasi
	bahan.Sayur = bahan.Sayur - butuhSayur
	bahan.Telur = bahan.Telur - butuhTelur
	fmt.Println(fmt.Sprintf("Membuat %d nasi goreng dan siap disajikan", jumlah))

	return bahan
}

func (bahan *Bahan) NasiGila(jumlah int) MenuFactory {
	// nasi gila pake 1 porsi nasi, 2 sayur, 1 telur
	var (
		butuhNasi  = jumlah * 1
		butuhSayur = jumlah * 2
		butuhTelur = jumlah * 1
	)

	if butuhNasi > bahan.Nasi || butuhSayur > bahan.Sayur || butuhTelur > bahan.Telur {
		bahan.ErrMsg = errors.New("Bahan tidak cukup untuk membuat nasi gila")
		return bahan
	}

	bahan.Nasi = bahan.Nasi - butuhNasi
	bahan.Sayur = bahan.Sayur - butuhSayur
	bahan.Telur = bahan.Telur - butuhTelur
	fmt.Println(fmt.Sprintf("Membuat %d nasi gila dan siap disajikan", jumlah))

	return bahan
}

func (bahan *Bahan) NasiCapcay(jumlah int) MenuFactory {
	// nasi capcay pake 1 porsi nasi, 3 sayur, 1 telur

	var (
		butuhNasi  = jumlah * 2
		butuhSayur = jumlah * 3
		butuhTelur = jumlah * 1
	)

	if butuhNasi > bahan.Nasi || butuhSayur > bahan.Sayur || butuhTelur > bahan.Telur {
		bahan.ErrMsg = errors.New("Bahan tidak cukup untuk membuat nasi capcay")
		return bahan
	}

	bahan.Nasi = bahan.Nasi - butuhNasi
	bahan.Sayur = bahan.Sayur - butuhSayur
	bahan.Telur = bahan.Telur - butuhTelur
	fmt.Println(fmt.Sprintf("Membuat %d nasi capcay dan siap disajikan", jumlah))

	return bahan
}

func (bahan *Bahan) CekBahanMakanan() MenuFactory {
	fmt.Println("Sisa bahan makanan")
	fmt.Println("Nasi: ", bahan.Nasi)
	fmt.Println("Sayur: ", bahan.Sayur)
	fmt.Println("Telur: ", bahan.Telur)
	return bahan
}

func (bahan *Bahan) Error() error {

	return bahan.ErrMsg
}
