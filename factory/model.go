package factory

type MenuFactory interface {
	NasiGoreng(jumlah int) MenuFactory
	NasiGila(jumlah int) MenuFactory
	NasiCapcay(jumlah int) MenuFactory
	CekBahanMakanan() MenuFactory
	Error() error
}

type Bahan struct {
	Nasi   int
	Sayur  int
	Telur  int
	ErrMsg error
}

func BahanMakanan(data ParamBahanMakanan) (MenuFactory, error) {
	return &Bahan{
		Nasi:  data.Nasi,
		Sayur: data.Sayur,
		Telur: data.Telur,
	}, nil
}
