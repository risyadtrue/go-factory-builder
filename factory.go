package main

import (
	"fmt"
	"go-factory-builder/factory"
)

func main() {
	dataBahanMakanan := factory.ParamBahanMakanan{
		Nasi:  8,
		Sayur: 8,
		Telur: 8,
	}

	menuMakanan, err := factory.BahanMakanan(dataBahanMakanan)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	err = menuMakanan.NasiGoreng(1).NasiGila(2).NasiCapcay(1).CekBahanMakanan().Error()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

}
