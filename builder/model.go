package builder

type BensinInterface interface {
	GetRON90() int
	GetRON92() int
	GetRON95() int
	GetRON98() (int, int)
}

type Pertamina struct {
	harga int
	liter int
}

func (p *Pertamina) GetRON90() int {
	p.harga = p.liter * 8000
	return p.harga
}

func (p *Pertamina) GetRON92() int {
	p.harga = p.liter * 9400
	return p.harga
}

func (p *Pertamina) GetRON95() int {
	p.harga = p.liter * 12000
	return p.harga
}

func (p *Pertamina) GetRON98() (int, int) {
	p.harga = p.liter * 14000
	return p.harga, p.liter
}

type Shell struct {
	harga int
	liter int
}

func (p *Shell) GetRON90() int {
	p.harga = p.liter * 10520
	return p.harga
}

func (p *Shell) GetRON92() int {
	p.harga = p.liter * 12900
	return p.harga
}

func (p *Shell) GetRON95() int {
	p.harga = p.liter * 13000
	return p.harga
}

func (p *Shell) GetRON98() (int, int) {
	p.harga = p.liter * 13750
	return p.harga, p.liter
}
