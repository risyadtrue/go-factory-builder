package builder

import "errors"

type DirectoryInterface interface {
	GetHargaBensin() BensinInterface
}

type Director struct {
	bensinBuilder BensinInterface
}

func (d *Director) GetHargaBensin() BensinInterface {
	return d.bensinBuilder
}

func IsiBensinByCompany(company string, data *ParamIsiBensin) (directory DirectoryInterface, err error) {
	var env BensinInterface

	switch company {
	case "Pertamina":
		env = &Pertamina{
			liter: data.Liter,
		}
	case "Shell":
		env = &Shell{
			liter: data.Liter,
		}
	default:
		return nil, errors.New("Company tidak terdaftar")

	}

	return &Director{env}, err
}
