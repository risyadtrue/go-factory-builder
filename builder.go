package main

import (
	"fmt"
	"go-factory-builder/builder"
)

func main() {
	isiBensin := builder.ParamIsiBensin{
		Liter: 5,
	}

	// builder
	d, err := builder.IsiBensinByCompany("Pertamina", &isiBensin)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(" harga pertamina RON90: ", d.GetHargaBensin().GetRON90())
	fmt.Println(" harga pertamina RON92: ", d.GetHargaBensin().GetRON92())
	fmt.Println(" harga pertamina RON95: ", d.GetHargaBensin().GetRON95())
	a, b := d.GetHargaBensin().GetRON98()
	fmt.Println(fmt.Sprintf("harga pertamina RON98 Per %d liter: %d", b, a))

	// builder
	ds, err := builder.IsiBensinByCompany("Shell", &isiBensin)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(" harga Shell RON90: ", ds.GetHargaBensin().GetRON90())
	fmt.Println(" harga Shell RON92: ", ds.GetHargaBensin().GetRON92())
	fmt.Println(" harga Shell RON95: ", ds.GetHargaBensin().GetRON95())
	ap, bp := d.GetHargaBensin().GetRON98()
	fmt.Println(fmt.Sprintf("harga pertamina RON98 Per %d liter: %d", bp, ap))

}
